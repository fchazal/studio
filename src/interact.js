export const Utils = {
	getBox: (element) => {
		const transform = Utils.getTransformations(element)

		return {
			top: parseFloat(element.style.top),
			left: parseFloat(element.style.left),
			width: parseFloat(element.style.width) * transform.scale,
			height: parseFloat(element.style.height) * transform.scale,
			transform: transform
		}
	},

	setBox: (element, box) => {
		const unit = 'px'
		Utils.setStyles(element,
		{
			'top': box.top + unit,
			'left': box.left + unit,
			'width': box.width / (box.transform ? box.transform.scale : 1) + unit,
			'height': box.height / (box.transform ? box.transform.scale : 1) + unit
		})

		if (box.transform) {
			Utils.setStyles(element,
			{
				'transform': `scale(${box.transform.scale}) rotate(${box.transform.rotation}deg)`
			}, true)
		}

		if ('createEvent' in document) {
			let e = document.createEvent('HTMLEvents')
			e.initEvent('resize', false, true)
			element.dispatchEvent(e)
		} else {
			element.dispatchEvent(new Event('resize'))
		}
	},

	getTransformations: el => {
		const st = window.getComputedStyle(el, null)
		const tr = st.getPropertyValue('-webkit-transform') ||
						st.getPropertyValue('-moz-transform') ||
						st.getPropertyValue('-ms-transform') ||
						st.getPropertyValue('-o-transform') ||
						st.getPropertyValue('transform') ||
						'none'

		let scale = 1
		let angle = 0

		if (tr != 'none') {
			const values = tr.split('(')[1].split(')')[0].split(',')
			const [a, b, c, d] = values

			scale = Math.round(Math.sqrt(a*a + b * b) * 1e4) / 1e4
			angle = Math.round(Math.atan2(b, a) * (180 / Math.PI))
		}

		return {
			scale: scale,
			rotation: angle
		}
	},

	isTouchCapable() {
    return 'ontouchstart' in window ||
      window.DocumentTouch && document instanceof window.DocumentTouch ||
      navigator.maxTouchPoints > 0 ||
      window.navigator.msMaxTouchPoints > 0
  },

	setStyles: (element, properties, prefix = false) => {
		const prefixes = [ '-o-', '-ms-', '-moz-', '-webkit-' ]

		for (var key in properties) {
			element.style[key] = properties[key]

			if (prefix) {
				prefixes.forEach(item => {
					element.style[item + key] = properties[key]
				})
			}
		}

		return element
	},

	toRad: deg => {
		return deg * Math.PI / 180
	},

	toDeg: rad => {
		return rad * Math.PI / 180
	}
}

export class Interaction {
	static create (selector, config = {}) {
		const object = typeof selector === 'string' ? document.querySelector(selector) : selector;
		return new Interaction(object, config)
	}

	static handle (event) {
		event.preventDefault()
		event.stopPropagation()
		event.returnValue = false
		try {
			return {
				clientX: event.clientX || event.targetTouches[0].clientX,
				clientY: event.clientY || event.targetTouches[0].clientY
			}
		} catch (e) {
			return {
				clientX: 0,
				clientY: 0
			}
		}
	}

	static get events () {
		return Utils.isTouchCapable() ? {
			start: 'touchstart',
			move: 'touchmove',
			end: 'touchend'
		} : {
			start: 'mousedown',
			move: 'mousemove',
			end: 'mouseup'
		}
	}

	static get touchCapable () {
		return 'ontouchstart' in window ||
			window.DocumentTouch && document instanceof window.DocumentTouch ||
			navigator.maxTouchPoints > 0 ||
			window.navigator.msMaxTouchPoints > 0
	}

	constructor (object, config = {}) {
		this.events = {}

		this.isDraggable = !!config.isDraggable
		this.isResizable = !!config.isResizable
		this.isRotatable = !!config.isRotatable

		this.data = {
			target: object,
			currentTarget: object,
			interaction: null
		}

		const pivot = { config } = [0, 0]
		this.pivot = {
			left: pivot[0],
			top: pivot[1]
		}

		this.target = object
		this.selected = false
		this.selection = new Selection(this)

		this.start = this.start.bind(this)
		this.move = this.move.bind(this)
		this.resize = this.resize.bind(this)
		this.rotate = this.rotate.bind(this)
		this.stop = this.stop.bind(this)
		this.unselect = this.unselect.bind(this)

		Utils.setStyles(this.target, {
			'transform-origin': `${this.pivot.left * 100}% ${this.pivot.top * 100}%`
		}, true)

		window.addEventListener(Interaction.events.end, this.unselect)
		window.addEventListener('unselect', this.unselect)
		window.addEventListener('resize', event => {
			if (this.selected) this.selection.update()
		})

		this.target.addEventListener('click', event => {
			this.select(event)
		})

		if (!Utils.isTouchCapable()) // on desktop avoid drag of the inner elements of the aperture
			this.target.addEventListener(Interaction.events.start, event => {
				event.preventDefault(); event.returnValue = false
			})

		this.selection.container.addEventListener(Interaction.events.start, this.start);
		return this
	}

	on (events, fn) {
		events.split(' ').forEach(event => {
			if (!this.events[event]) this.events[event] = []
			this.events[event].push(fn)
		})

		return this
	}

	dispatch (event) {
		(this.events[event] || []).forEach(fn => {
			fn(this.data)
		})
	}

	select (event) {
		Interaction.handle(event)

		if (!this.selected) {
			if ('createEvent' in document) {
				let e = document.createEvent('HTMLEvents')
				e.initEvent('unselect', false, true)
				window.dispatchEvent(e)
			} else {
				window.dispatchEvent(new Event('unselect'))
			}

			this.selected = true
			this.selection.update(this.target)
			document.body.appendChild(this.selection.container)
		}
	}

	unselect () {
		if (this.selected) {
			document.body.removeChild(this.selection.container)
			this.selected = false
		}
	}

	start (event) {
		const position = Interaction.handle(event)

		this.data.interaction = null
		window.removeEventListener(Interaction.events.end, this.unselect)

		if (this.isResizable && event.target.classList.contains('resize')) {
			this.data.interaction = 'resize'
			this.data.edges = {
				top: event.target.classList.contains('top'),
				left: event.target.classList.contains('left'),
				right: event.target.classList.contains('right'),
				bottom: event.target.classList.contains('bottom')
			}
			window.addEventListener(Interaction.events.move, this.resize)
		} else if (this.isRotatable && event.target.classList.contains('rotate')) {
			this.data.interaction = 'rotate'
			window.addEventListener(Interaction.events.move, this.rotate)
		} else if (this.isDraggable) {
			this.data.interaction = 'move'
			window.addEventListener(Interaction.events.move, this.move)
		}

		window.addEventListener(Interaction.events.end, this.stop)

		this.dispatch('down')
		this.selection.update()

		this.initX = position.clientX
		this.initY = position.clientY

		this.initTransform = Utils.getTransformations(this.target)
		this.data.rotated = this.initTransform.rotation != 0

		this.initTop = this.target.offsetTop
		this.initLeft = this.target.offsetLeft
		this.initWidth = this.target.offsetWidth * this.initTransform.scale
		this.initHeight = this.target.offsetHeight * this.initTransform.scale

		const angle = Utils.toRad(this.initTransform.rotation)
		this.pivotX = this.selection.left + Math.cos(angle) * this.initWidth * this.pivot.left - Math.sin(angle) * this.initHeight * this.pivot.top
		this.pivotY = this.selection.top + Math.sin(angle) * this.initWidth * this.pivot.left + Math.cos(angle) * this.initHeight * this.pivot.top
	}

	stop (event) {
		const position = Interaction.handle(event)

		this.isRotatable && window.removeEventListener(Interaction.events.move, this.rotate)
		this.isResizable && window.removeEventListener(Interaction.events.move, this.resize)
		this.isDraggable && window.removeEventListener(Interaction.events.move, this.move)
		window.removeEventListener(Interaction.events.end, this.stop)

		this.dispatch('up')
		this.selection.update()

		window.addEventListener(Interaction.events.end, this.unselect)
	}

	move (event) {
		const position = Interaction.handle(event)

		const dX = position.clientX - this.initX
		const dY = position.clientY - this.initY

		this.data.rect = {
			top: this.initTop + dY,
			left: this.initLeft + dX
		}

		this.dispatch('move')
		this.selection.update()
	}

	resize (event) {
		const position = Interaction.handle(event)
		const angle = Utils.toRad(this.initTransform.rotation)

		const dX = position.clientX - this.initX
		const dY = position.clientY - this.initY

		let pL = this.pivot.left
		let pT = this.pivot.top

		let dL = 0
		let dT = 0
		let dW = 0
		let dH = 0

		//  A +----------+ B
		//    |          |
		//    |  + P     |
		//    |          |
		//  C +----------+ D

		if (this.data.edges.right) { // A and C are fixed
			dW = Math.cos(angle) * dX + Math.sin(angle) * dY

			dL += (Math.cos(angle) - 1) * dW * pL
			dT += (Math.sin(angle)) * dW * pL
		}

		if (this.data.edges.bottom) { // A and B are fixed
			dH = -Math.sin(angle) * dX + Math.cos(angle) * dY

			dL += -Math.sin(angle) * dH * pT
			dT += (Math.cos(angle) - 1) * dH * pT
		}

		if (this.data.edges.left) { // B and D are fixed
			dW = -(Math.cos(angle) * dX + Math.sin(angle) * dY)

			dL += -dW * (pL + (1 - pL) * Math.cos(angle))
			dT += -dW * (1 - pL) * Math.sin(angle)
		}

		if (this.data.edges.top) { // C and D are fixed
			dH = -(-Math.sin(angle) * dX + Math.cos(angle) * dY)

			dL += dH * ((1 - pT) * Math.sin(angle))
			dT += -dH * (pT + (1 - pT) * Math.cos(angle))
		}

		this.data.rect = {
			top: this.initTop + dT,
			left: this.initLeft + dL,
			width: this.initWidth + dW,
			height: this.initHeight + dH
		}

		this.dispatch('resize')
		this.selection.update()
	}

	rotate (event) {
		const position = Interaction.handle(event)

		const dX = position.clientX - this.pivotX
		const dY = position.clientY - this.pivotY

		const angle = (90 - Math.atan(dY / dX) * 180 / Math.PI) + (dX >= 0 ? 180 : 0)

		this.data.rotate = -Math.round(angle / 5) * 5 // by 5° steps
		this.data.rotated = this.data.rotate != 0

		this.dispatch('rotate')
		this.selection.update()
	}
}

export class Selection {
	constructor (interaction) {
		this.target = interaction.target
		this.elements = []

		this.container = this.newElement('interact-selection');

		if (interaction.isResizable) {
			['top', 'left', 'right', 'bottom', 'top left', 'top right', 'bottom left', 'bottom right'].forEach(className =>
		  this.container.appendChild(this.newElement(`interact-anchor resize ${className}`)));
		}

		if (interaction.isRotatable) {
			this.container.appendChild(
				Utils.setStyles(this.newElement('interact-anchor rotate top'), {
					left: interaction.pivot.left * 100 + '%'
				})
			)

			this.container.appendChild(
				Utils.setStyles(this.newElement('interact-pivot'), {
					top: interaction.pivot.top * 100 + '%',
					left: interaction.pivot.left * 100 + '%'
				})
			)
		}

		return this
	}

  newElement(className) {
    const div = document.createElement('div')
    div.className = className
    return div
  }

	update () {
		const box = Utils.getBox(this.target)
		const rect = this.target.getBoundingClientRect()

		const deg = box.transform.rotation
		const rad = Utils.toRad(box.transform.rotation)

		let dX = 0
		let dY = 0

		if (deg < 0 && deg >= -90) {
			dY = -Math.sin(rad) * box.width
		} else if (deg >= 0 && deg < 90) {
			dX = Math.sin(rad) * box.height
		} else if (deg >= 90) {
			dY = -Math.cos(rad) * box.height
			dX = rect.width
		} else if (deg < -90) {
			dY = rect.height
			dX = -Math.cos(rad) * box.width
		}

		this.top = rect.top + dY
		this.left = rect.left + dX

		Utils.setStyles(this.container, {
			'top': rect.top + dY + 'px',
			'left': rect.left + dX + 'px',
			'width': box.width + 'px',
			'height': box.height + 'px'
		})

		Utils.setStyles(this.container, {
			'transform-origin': '0% 0%',
			'transform': `rotate(${box.transform.rotation}deg)`
		}, true);
	}
}

export default Interaction
