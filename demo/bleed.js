var bleed = {
	top: 3,
	right: 3,
	bottom: 3,
	left: 3,

	snap: (box) => {
		const position = grid.position(box)

		if (Math.abs(position.left) < 0.5) {
			box.left = 0
			box.width += convert.display(bleed.left)
		}
		
		if (Math.abs(position.right) < 0.5) {
			box.width += convert.display(bleed.right)
		}

		if (Math.abs(position.top) < 0.5) {
			box.top = 0
			box.height += convert.display(bleed.top)
		}

		if (Math.abs(position.bottom) < 0.5) {
			box.height += convert.display(bleed.bottom)
		}

		return box
	},

	unsnap: (box) => {
		const position = grid.position(box)

		if (position.left < 0) {
			box.left = convert.display(bleed.left)
			box.width -= convert.display(bleed.left)
		}
		
		if (position.right < 0) {
			box.width -= convert.display(bleed.right)
		}

		if (position.top < 0) {
			box.top = convert.display(bleed.top)
			box.height -= convert.display(bleed.top)
		}

		if (position.bottom < 0) {
			box.height -= convert.display(bleed.bottom)
		}
		
		return box
	}
}