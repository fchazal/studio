const { Utils, Interaction } = interact

const unit = 'px'

const convert = {
	print: 		(float) => { return float / page.zoom },
	display: 	(float) => { return parseFloat((float * page.zoom).toFixed(2)) },
	pixel: 		(float) => { return convert.display(float) + unit },
}

window.onload = () => {
	page.initialize()
	grid.initialize()

	new PhotoAperture({ top: 0, left: 0, width: 70, height: 40 , transform: { scale: 1, rotation: 0 }}, 'images/photo1.jpg')
	new PhotoAperture({ top: 0, left: 70, width: 70, height: 40, transform: { scale: 1, rotation: 0 }}, 'images/photo2.jpg')
	new PhotoAperture({ top: 66, left: 0, width: 70, height: 40, transform: { scale: 1, rotation: 0 }}, 'images/photo3.jpg')
	new PhotoAperture({ top: 66, left: 70, width: 70, height: 40, transform: { scale: 1, rotation: 0 }}, 'images/photo4.jpg')

	new TextAperture({ top: 42, left: 20, width: 100, height: 22, transform: { scale: 0.5, rotation: 0 }}, `<p>Il n'y a qu'une façon de dire oui, c'est «oui», toutes les autres veulent dire non.</p><p><b>Charles Maurice, prince de Talleyrand-Périgord</b></p>`)
}

/*
	window.location.hash="no-back-button";
	window.location.hash="Again-No-back-button";//again because google chrome don't insert first hash into history
	window.onhashchange=function(){
		window.location.hash="no-back-button";
		alert('NOOOOO')
	}
*/
