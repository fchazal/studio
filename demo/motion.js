const motion = {
	initialize: (selector, pivot = [0, 0]) => {
		Interaction.create(selector, {
            isResizable: true,
            isDraggable: true,
            isRotatable: true,
            pivot
        })
			.on('down', motion.capture)
			.on('move', motion.move)
			.on('resize', motion.resize)
			.on('rotate', motion.rotate)
			.on('up', motion.release)
	},

	capture: (event) => {
		motion.target = event.currentTarget

		let box = Utils.getBox(motion.target)

		if (!event.rotated) {
			if (motion.target.classList.contains('photo'))
				bleed.unsnap(box)
		}

		Utils.setBox(motion.target, box)
	},

	release: (event) => {
		if (!motion.target) return
		let box = Utils.getBox(motion.target)

		if (!event.rotated) {
			grid.snap(box)

			if (motion.target.classList.contains('photo'))
				bleed.snap(box)
		}

		Utils.setBox(motion.target, box)

		motion.target = null
	},

	move: ({ rect, rotated }) => {
		let box = Utils.getBox(motion.target)

		box.top = rect.top
		box.left = rect.left

		if (!rotated) {
			motion.constrain(box, false)
			grid.snap(box)
		}

		Utils.setBox(motion.target, box)
	},

	resize: ({ rect, edges, rotated }) => {
		let box = Utils.getBox(motion.target)

		box.top = rect.top
		box.left = rect.left
		box.width = rect.width
		box.height = rect.height

		if (!rotated) {
			motion.constrain(box, true)
			grid.snap(box)
		}

		if (motion.target.classList.contains('text'))
			box = TextAperture.constrain(motion.target, box)

		Utils.setBox(motion.target, box)
	},

	rotate: ({ rotate }) => {
		let box = Utils.getBox(motion.target)
		box.transform.rotation = rotate

		Utils.setBox(motion.target, box)
	},

	constrain(box, resize = false) {
		let bounds = page.getBoundingBox()

		if (box.top < bounds.Ymin) {
			if (resize) box.height = box.height - (bounds.Ymin - box.top)
			box.top = bounds.Ymin
		}

		if (box.left < bounds.Xmin) {
			if (resize) box.width = box.width - (bounds.Xmin - box.left)
			box.left = bounds.Xmin
		}

		if (box.left + box.width > bounds.Xmax) {
			if (!resize) box.left = bounds.Xmax - box.width
			if (resize) box.width = box.width - (box.left + box.width - bounds.Xmax)
		}

		if (box.top + box.height > bounds.Ymax) {
			if (!resize) box.top = bounds.Ymax - box.height
			if (resize) box.height = box.height - (box.top + box.height - bounds.Ymax)
		}

		return box
	}
}
