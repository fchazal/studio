const grid = {
	steps: 100,

	initialize: () => {
		const innerWidth = page.width - bleed.left - bleed.right
		const innerHeight = page.height - bleed.top - bleed.bottom

		grid.sizeX = grid.gcd(page.width, page.height) //innerWidth / grid.steps
		grid.sizeY = grid.sizeX //innerHeight / grid.steps
	},

	gcd: (a, b) => {
    if (!b) return a
    return grid.gcd(b, a % b)
	},

	position: (box) => {
		const bounding = page.getBoundingBox()

		return {
			top: convert.print(box.top - bounding.Ymin) / grid.sizeY,
			left: convert.print(box.left - bounding.Xmin) / grid.sizeX,
			
			right2: convert.print(box.left + box.width - bounding.Xmin) / grid.sizeX,
			bottom2: convert.print(box.top + box.height - bounding.Ymin) / grid.sizeY,
			
			width: convert.print(box.width) / grid.sizeX,
			height: convert.print(box.height) / grid.sizeY,

			right: convert.print(bounding.Xmax - box.left - box.width) / grid.sizeX,
			bottom: convert.print(bounding.Ymax - box.top - box.height) / grid.sizeY
		}
	},

	snap: (box) => {
		const position = grid.position(box)

		box.top = convert.display(Math.round(position.top) * grid.sizeY + bleed.top)
		box.left = convert.display(Math.round(position.left) * grid.sizeX + bleed.left)
		box.right = convert.display(Math.round(position.right2) * grid.sizeX + bleed.left)
		box.bottom = convert.display(Math.round(position.bottom2) * grid.sizeY + bleed.top)

		box.width = box.right - box.left
		box.height = box.bottom - box.top

		return box
	}
}
