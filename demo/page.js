const page = {
	zoom: 3,

	width: 286,
	height: 218,

	initialize: () => {
		Array.prototype.forEach.call(document.querySelectorAll('.greatparent'), (greatparent) => {
			Utils.setStyles(greatparent,
			{
				'width': convert.pixel(page.width - bleed.left - bleed.right),
				'height': convert.pixel(page.height - bleed.top - bleed.bottom)
			})
		})

		Array.prototype.forEach.call(document.querySelectorAll('.parent'), (parent) => {
			Utils.setStyles(parent,
			{
				'top': convert.pixel(-bleed.top),
				'left': convert.pixel(-bleed.left),
				'width': convert.pixel(page.width),
				'height': convert.pixel(page.height)
			})

			page.container = parent
		})

		Array.prototype.forEach.call(document.querySelectorAll('.background'), (background) => {
			Utils.setStyles(background,
			{
				'top': convert.pixel(bleed.top),
				'left': convert.pixel(bleed.left),
				'right': convert.pixel(bleed.right),
				'bottom': convert.pixel(bleed.bottom)
			})
		})
	},

	getBoundingBox: () => {
    return {
			Xmin: convert.display(bleed.left),
			Ymin: convert.display(bleed.top),
			Xmax: convert.display(page.width - bleed.right),
			Ymax: convert.display(page.height - bleed.bottom)
		}
	}
}
