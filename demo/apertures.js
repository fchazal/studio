class Aperture {
	constructor(coords, selector = 'aperture', pivot = [0, 0]) {
		Aperture.id = Aperture.id++ || 0
		this.view = document.createElement('div')

		this.view.id = `aperture_${Aperture.id++}`
		this.view.classList.add('aperture')
		this.view.classList.add(selector)

		let box = {
			top: convert.display(bleed.top + coords.top * grid.sizeY),
			left: convert.display(bleed.left + coords.left * grid.sizeX),
			width: convert.display(coords.width * grid.sizeX),
			height: convert.display(coords.height * grid.sizeY),
			transform: coords.transform
		}

		motion.constrain(box, true)
		grid.snap(box)
		bleed.snap(box)

		Utils.setBox(this.view, box)

		switch (selector) {
			case 'photo':
				page.container.querySelector('.photos').appendChild(this.view)
				break
			case 'text':
				page.container.querySelector('.texts').appendChild(this.view)
				break
			default:
				page.container.appendChild(this.view)
		}
		motion.initialize(`#${this.view.id}`, pivot)
	}

}

class PhotoAperture extends Aperture {
	constructor(box, url) {
		super(box, 'photo', [ 0.5, 0.5 ])

		this.crop = { top: 0, left: 0, right: 1, bottom: 1 }
		this.image = document.createElement('img')
		this.image.onload = () => { this.optimizeImage(); this.updateCrop() }
		this.view.appendChild(this.image)
		this.image.src = url

		this.view.addEventListener('resize', () => { this.optimizeImage() })
	}

	optimizeImage() {
		const aperture_box = Utils.getBox(this.view)
		let crop_box = {}

		const crop_width = (this.crop.right - this.crop.left)
		const crop_height = (this.crop.bottom - this.crop.top)
		const aperture_ratio = aperture_box.width / aperture_box.height
		const crop_ratio = crop_width / crop_height

		if (aperture_ratio > crop_ratio) {
			crop_box.width = aperture_box.width
			crop_box.height = crop_box.width / crop_ratio
		} else {
			crop_box.height = aperture_box.height
			crop_box.width = crop_box.height * crop_ratio
		}

		crop_box.left = (aperture_box.width - crop_box.width) / 2
		crop_box.top = (aperture_box.height - crop_box.height) / 2

		let image_box = {
			width: crop_box.width / crop_width,
			height: crop_box.height / crop_height
		}

		image_box.top = -this.crop.top * image_box.width + crop_box.top
		image_box.left = -this.crop.left * image_box.height + crop_box.left

		Utils.setBox(this.image, image_box)
	}

	updateCrop() {
		const aperture_box = Utils.getBox(this.view)
		const image_box = Utils.getBox(this.image)

		this.crop.top = (-image_box.top) / image_box.height
		this.crop.left = (-image_box.left) / image_box.width
		this.crop.right = (aperture_box.width-image_box.left) / image_box.width
		this.crop.bottom = (aperture_box.height-image_box.top) / image_box.height
	}
}

class TextAperture extends Aperture {
	constructor(box, content) {
		super(box, 'text', [ 0, 0 ])
		this.view.classList.add('text')

		this.text = document.createElement('div')
		this.text.classList.add('container')
		this.text.innerHTML = content

		this.view.appendChild(this.text)

//		this.view.addEventListener('resize', () => { this.optimizeText() })
	}

	static constrain(element, box) {
		const origin = Utils.getBox(element)

		let tmp = element.cloneNode(true)
		tmp.style.visibility = 'hidden'

		element.parentNode.appendChild(tmp)
		Utils.setBox(tmp, box)

		if ((tmp.offsetHeight < tmp.scrollHeight) || (tmp.offsetWidth < tmp.scrollWidth)) {
			box = origin
		}

		element.parentNode.removeChild(tmp)

		return box
	}
}
