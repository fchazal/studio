import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import babelrc from 'babelrc-rollup';
import minify from 'rollup-plugin-babel-minify';
import pkg from './package.json';

const env = process.env.ROLLUP_ENV;
const libraryName = 'interact';
const outputFile = (env === 'build') ? libraryName + '.min.js' : libraryName + '.js';

const plugins = [
	resolve(),
	commonjs(),
	babel(babelrc({
		exclude: ['node_modules/**']
	}))
];

if (env === 'build') {
	plugins.push(minify());
}

export default [
	// browser-friendly UMD build
	{
		input: 'src/interact.js',
		output: {
			file: 'dist/' + outputFile,
			name: libraryName,
			format: 'umd',
			exports: 'named'
		},
		plugins: plugins
	}
];
